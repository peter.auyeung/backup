#!/usr/bin/env python3
'''
offsite.py offsite local backup to cloud

1. take lvm snapshots
2. mount snapshots
3. rclone snapshots to cloud
4. umount snapshots
5. remove lvm snapshots
'''

import argparse
import configparser
import subprocess
import sys
import json
import urllib.request
import urllib.error
import urllib.parse
import logging
import os

logging.basicConfig(
    level=logging.INFO,
    format='[%(levelname)s] %(asctime)s %(message)s',
    datefmt='%b %d %Y %H:%M:%S')
logger = logging.getLogger(__name__)


class OffsiteConfig(object):
    '''Collecting offsite configurations
    1. backup server
    2. offsite policy json
    3. lvm information
    '''
    def __init__(self, conf, debug=False):
        self.debug = debug
        self.raw_conf = conf

        # Unique to Json
        self.id_ = conf.get('id', '')
        self.site = conf.get('site', '')
        self.storage = conf.get('storage', '')
        self.cloudid = conf.get('cloudid', '')

        if self.debug:
            print("storage: {}".format(self.storage))


class OffsiteProcessor(object):
    '''Run all offsite process
    run_offsite
        for lvmvg cache
            create_lvm_snapshot
            mount_lvm_snapshot
            rclone_snapshot_to_cloud
            umount_lvm_snapshot
            remove_lvm_snapshot
    '''
    def __init__(self, job_conf, debug=False):
        self.debug = debug
        self.job_conf = OffsiteConfig(job_conf, debug)
        self.lvcreate_cmd_tmpl = ('/sbin/lvcreate --size {size} -s -n '
                                  '{lv} {dev}')
        self.mount_cmd_tmpl = ('/bin/mount -t xfs -o nouuid {dev} {path}')
        self.rclone_cmd_tmpl = ('/usr/bin/rclone '
                                '--verbose '
                                '--fast-list '
                                '--transfers 8 sync '
                                '{path} {cloudid}:{bucket}/')
        self.umount_cmd_tmpl = ('/bin/umount -f {path}')
        self.lvremove_cmd_tmpl = ('/sbin/lvremove --force {dev}')

    def create_lvm_snapshot(self, snapshotsize, lvmlv, lvmvg):
        snapshotlv = '{}-snapshotlv'.format(lvmlv)
        sourcedev = '/dev/{}vg/{}'.format(lvmvg, lvmlv)
        lvcreate = self.lvcreate_cmd_tmpl.format(size=snapshotsize,
                                                 lv=snapshotlv,
                                                 dev=sourcedev)
        ret = 0
        if self.debug:
            print(lvcreate)
        else:
            ret = subprocess.call(lvcreate, shell=True)
            if ret != 0:
                logger.error('Error create snapshot of %s', sourcedev)
        return True if ret == 0 else False

    def mount_lvm_snapshot(self, lvmlv, lvmvg):
        snapshotdev = '/dev/{}vg/{}-snapshotlv'.format(lvmvg, lvmlv)
        snapshotpath = '/{}/{}-snapshot'.format(lvmvg, lvmlv)
        if not os.path.exists(snapshotpath):
            try:
                os.makedirs(snapshotpath)
            except OSError:
                logger.error('Error mkdir %s', snapshotpath)
                raise
        mount = self.mount_cmd_tmpl.format(dev=snapshotdev,
                                           path=snapshotpath)
        ret = 0
        if self.debug:
            print(mount)
        else:
            ret = subprocess.call(mount, shell=True)
            if ret != 0:
                logger.error('Error mounting snapshot of %s', snapshotpath)
        return True if ret == 0 else False

    def rclone_snapshot_to_cloud(self, lvmvg, lvmlv, cloudid, bucket):
        snapshotpath = '/{}/{}-snapshot'.format(lvmvg, lvmlv)
        rclone = self.rclone_cmd_tmpl.format(path=snapshotpath,
                                             cloudid=cloudid,
                                             bucket=bucket)
        ret = 0
        print(bucket)
        if self.debug:
            print(rclone)
        else:
            ret = subprocess.call(rclone, shell=True)
            if ret != 0:
                logger.error('Error on rclone %s', snapshotpath)
        return True if ret == 0 else False

    def umount_lvm_snapshot(self, lvmlv, lvmvg):
        snapshotpath = '/{}/{}-snapshot'.format(lvmvg, lvmlv)
        umount = self.umount_cmd_tmpl.format(path=snapshotpath)
        ret = 0
        if self.debug:
            print(umount)
        else:
            ret = subprocess.call(umount, shell=True)
            if ret != 0:
                logger.error('Error unmounting snapshot of %s', snapshotpath)
        return True if ret == 0 else False

    def remove_lvm_snapshot(self, lvmlv, lvmvg):
        snapshotdev = '/dev/{}vg/{}-snapshotlv'.format(lvmvg, lvmlv)
        lvremove = self.lvremove_cmd_tmpl.format(dev=snapshotdev)
        ret = 0
        if self.debug:
            print(lvremove)
        else:
            ret = subprocess.call(lvremove, shell=True)
            if ret != 0:
                logger.error('Error remove snapshot of %s', snapshotdev)
        return True if ret == 0 else False

    def run_offsite(self):
        for _, storage_dict in self.job_conf.storage.items():
            self.create_lvm_snapshot(storage_dict['snapshotsize'],
                                     storage_dict['lvmlv'],
                                     storage_dict['lvmvg'])
            self.mount_lvm_snapshot(storage_dict['lvmlv'],
                                    storage_dict['lvmvg'])
        for _, storage_dict in self.job_conf.storage.items():
            self.rclone_snapshot_to_cloud(storage_dict['lvmvg'],
                                          storage_dict['lvmlv'],
                                          self.job_conf.cloudid,
                                          storage_dict['bucket'])
        for _, storage_dict in self.job_conf.storage.items():
            self.umount_lvm_snapshot(storage_dict['lvmlv'],
                                     storage_dict['lvmvg'])
            self.remove_lvm_snapshot(storage_dict['lvmlv'],
                                     storage_dict['lvmvg'])
        return True


def get_job_config(git_url, git_hash, job_folder, conf_name):
    ''' Load JSON configs file from git
    git_url example:
    job_folder example: StorageOps
    '''
    conf_url = '%s%s/%s/%s' % \
        (git_url, git_hash, job_folder, conf_name)
    try:
        response = urllib.request.urlopen(conf_url)
    except urllib.URLError:
        logger.error('Unable to open %s', conf_url)
        raise
    try:
        conf = json.loads(response.read().decode('utf-8'))
    except ValueError:
        logger.error('Unable to parse JSON in %s', conf_url)
        raise
    return conf


def cli_runner():
    '''collect command line arguments and kick off run_offsite'''
    parser = argparse.ArgumentParser(description='Offsite Process')
    parser.add_argument('-l', '--lob', dest='line_of_business',
                        default="Offsite", help='git directory',
                        required=False)
    parser.add_argument('-f', '--conf_name', dest='conf_name',
                        default=None, help='JSON config filename in git',
                        required=True)
    parser.add_argument('-v', '--git_hash', dest='git_hash',
                        default=None, help='Hash version of the file',
                        required=True)
    parser.add_argument('-c', '--config_file', dest='config_file',
                        default="/etc/backup/backup.ini",
                        help='Backup server config file',
                        required=False)
    parser.add_argument('-d', '--debug', dest='debug',
                        default=False, help='turn debug on/off',
                        required=False)

    # Parsing arguements
    args = parser.parse_args()
    # Parsing backup server config
    config = configparser.ConfigParser()
    config.read(args.config_file)
    giturl = config.get('job_config', 'GIT_URL')
    jobfolder = args.line_of_business
    jobconf = {}
    jobconf.update(get_job_config(giturl,
                                  args.git_hash,
                                  jobfolder,
                                  args.conf_name
                                  )
                   )
    # Pass backup server config info job config
    offsite_processor = OffsiteProcessor(jobconf, args.debug)
    return offsite_processor.run_offsite()


if __name__ == "__main__":
    if cli_runner():
        sys.exit(0)
    else:
        sys.exit(1)
