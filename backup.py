#!/usr/bin/env python3
"""
backup.py execute backup and offsite jobs

backup jobs:
    1. It pulls backup policy json files from git
    2. Determine Backup repo and source locations,
       mounting sshfs from client if necessary
    3. borg backup each filesystems from client policy
    4. Unmount sshfs if necessary when completed
    5. Prune old backups
"""
import configparser
import argparse
import json
import logging
import os
import paramiko
import subprocess
import sys
import time
import urllib.request, urllib.error, urllib.parse
from datetime import datetime

logging.basicConfig(
    level=logging.INFO,
    format='[%(levelname)s] %(asctime)s %(message)s',
    datefmt='%b %d %Y %H:%M:%S')


class BackupConfig(object):
    """Collecting necessary backup Configrautons
    1. configuration of bacup server
    2. backup policy json
    3. filesystem information
    """

    def __init__(self, conf, backup_type, debug=False,
                 fskey='',
                 source='/data',
                 destination='/data'):
        self.logger = logging.getLogger(__name__)
        self.debug = debug
        self.raw_conf = conf
        self.backup_type = backup_type
        self.borg_passphrase = conf.get('borg_passphrase', '')
        self.borg_cache_dir = conf.get('borg_cache_dir', '')
        self.borg_files_cache_ttl = conf.get('borg_files_cache_ttl', '')
        self.tmpdir = conf.get('tmpdir', '')
        self.private_key = conf.get('private_key', '')
        self.local_path = conf.get('local_path', '')
        self.offsite_path = conf.get('offsite_path', '')
        self.nfs_base_path = conf.get('nfs_base_path', '/gfs')
        self.sshfs_base_path = conf.get('sshfs_base_path', '/sshfs')
        self.offsite = conf.get('offsite', '')
        self.repocheck = conf.get('repocheck', '')
        self.threads = conf.get('threads', '')
        self.exclude = conf.get('exclude', '')

        # Unique to  Json
        self.id_ = conf.get('id', '')
        self.site = conf.get('site', '')
        self.hosts = conf.get('hosts', [])
        self.daystokeep = conf.get('daystokeep', '')
        self.filesystems = conf.get('filesystems', {})

        # Unique to filesystem
        self.path = conf.get('path', '')
        self.fskey = fskey
        self.source = source
        self.destination = destination

        if not self.is_valid():
            valid_types = {'local', 'offsite'}
            if self.backup_type not in valid_types:
                self.logger.error('[%s] is not a valid backup type',
                                  self.backup_type)

    @property
    def backup_root(self):
        """concatenating backup root path"""
        path = '{}_path'.format(self.backup_type)
        try:
            return getattr(self, path)
        except AttributeError:
            return ''

    def is_valid(self):
        """Directory path validation
        valid by path string and os.path.isdir
        """
        string_list = [
            self.nfs_base_path,
            self.sshfs_base_path,
            self.source,
            self.destination,
        ]
        dir_list = [
            self.nfs_base_path,
            self.sshfs_base_path,
            self.source,
            self.destination,
        ]
        return (
                all([isinstance(s, str) for s in string_list]) and
                all([os.path.isdir(d) for d in dir_list]) and
                all([os.path.exists(d) for d in dir_list])
        )


class BackupProcessor(object):
    """Run all backup processes
    run_backup
    for each host
        run backup_host
        for each filesystem
            run Backup_fs
                run get_source_destination
                run sync_path
        if success
            run take_backup_snapshot
    """

    def __init__(self, job_conf, backup_type, debug=False):
        self.logger = logging.getLogger(__name__)
        self.job_conf = BackupConfig(job_conf, backup_type, debug)
        self.borg_passphrase = job_conf.get('borg_passphrase', '')
        self.borg_cache_dir = job_conf.get('borg_cache_dir', '')
        self.borg_files_cache_ttl = job_conf.get('borg_files_cache_ttl', '')
        self.tmpdir = job_conf.get('tmpdir', '')
        self.sshfs_base_path = job_conf.get('sshfs_base_path', '')
        self.backup_type = backup_type
        self.debug = debug
        self.borginit_cmd_tmpl = '/usr/bin/borg init {repo}'
        self.borgcheck_cmd_tmpl = ('/usr/bin/borg check '
                                   '--repository-only {repo}')
        self.borgcreate_cmd_tmpl = ('/usr/bin/borg create -v --stats '
                                    '--compression zlib,9 {repo}::{arch} '
                                    '{includes} {excludes}')
        self.borgprune_cmd_tmpl = ('/usr/bin/borg prune -v {repo} '
                                   '--keep-daily={daystokeep}')
        self.sshfs_cmd_tmpl = ('/usr/bin/sshfs -o ro -o compression=no '
                               '-o Ciphers=arcfour root@{server}:{dir_} {mtpt}')
        self.borg_env = {"BORG_PASSPHRASE": self.borg_passphrase,
                         "BORG_CACHE_DIR": self.borg_cache_dir,
                         "BORG_FILES_CACHE_TTL": self.borg_files_cache_ttl,
                         "TMPDIR": self.tmpdir}

    @staticmethod
    def list_get(list_, index, default):
        """helper function to validate dictionary length"""
        return list_[index] if len(list_) > index else default

    @staticmethod
    def listdir_not_empty(directory):
        """Make sure directory is not empty"""
        return os.path.isdir(directory) and os.listdir(directory) != ""

    @staticmethod
    def valid_date(date_text):
        """Make sure string is a valid date format"""
        try:
            datetime.strptime(date_text, '%Y%m%d')
            return True
        except ValueError:
            return False

    def get_fstype_fspath(self, host, path, pri_key):
        """Figure out filesystem type for backup
        and corresponding path to be backup
        """
        sshkey = paramiko.RSAKey.from_private_key_file(pri_key)
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.load_system_host_keys()
        ssh.connect(hostname=str(host), username='root', pkey=sshkey)
        _, stdout, _ = ssh.exec_command('stat -f -L -c %%T %s' % path)
        fs_type = stdout.read().decode('utf-8').replace("\n", "")
        fs_path = path
        # Determine NFS mount point
        if fs_type == "nfs":
            # Check if path is a link
            _, stdout, _ = ssh.exec_command('ls -ld %s' % path)
            ls_path = stdout.read().decode('utf-8').split()
            mod = self.list_get(ls_path, 0, 0)
            if mod.startswith('l'):
                try:
                    path = self.list_get(ls_path, 10, 10)
                except:
                    self.logger.error('Cannot stat symlink %s!', path)
                    raise
            command = 'df %s | sed -e /Filesystem/d' % path
            _, stdout, _ = ssh.exec_command(command)
            df_ = stdout.read().decode('utf-8').split()
            try:
                mountpt = self.list_get(df_, 5, 5)
                nfssrc = self.list_get(df_, 0, 0).split(":")[1]
                fs_path = nfssrc + path.split(mountpt)[1]
            except:
                self.logger.error('Cannot stat client path %s!', path)
                raise
        ssh.close()
        return fs_type, fs_path

    def get_repo_path(self, backup_root, policy_id):
        """Define borg repo path"""
        directory = os.path.join(backup_root, policy_id)
        if not os.path.exists(directory):
            self.init_borg_repo(directory)
        if not os.path.exists(self.borg_cache_dir):
            os.makedirs(self.borg_cache_dir)
        if not os.path.exists(self.tmpdir):
            os.makedirs(self.tmpdir)
        if self.debug:
            print(self.borgcheck_cmd_tmpl.format(repo=directory))
        return directory

    def run_borgcheck_repo(self, backup_repo):
        """Check integrity of borg if required"""
        command = self.borgcheck_cmd_tmpl.format(repo=backup_repo)
        if self.job_conf.repocheck:
            ret = subprocess.call(command,
                                  env=self.borg_env,
                                  shell=True)
            if ret != 0:
                self.logger.info('Repo %s check failed', backup_repo)

    def init_borg_repo(self, path):
        """Initialize borg repo"""
        if self.debug:
            print(self.borginit_cmd_tmpl.format(repo=path))
        else:
            command = self.borginit_cmd_tmpl.format(repo=path)
            ret = subprocess.call(command,
                                  env=self.borg_env,
                                  shell=True)
            if ret != 0:
                self.logger.error('Cannot initialize repo %s', path)
                sys.exit(1)

    def get_remote_path(self, fs_type, policy, path, site):
        """Mapping client filesystems to backup server locally
        either via sshfs or glusterfs
        """
        if fs_type == "nfs":
            directory = os.path.join(self.job_conf.nfs_base_path,
                                     site,
                                     path.lstrip("/"))
        else:
            directory = os.path.join(self.job_conf.sshfs_base_path,
                                     site,
                                     policy,
                                     path.lstrip("/"))
            if not os.path.exists(directory):
                os.makedirs(directory)
        return directory

    def mount_sshfs(self, host, path, mount_point):
        """Mounting backup client filesystem via sshfs"""
        if self.debug:
            print(self.sshfs_cmd_tmpl.format(server=host,
                                             dir_=path,
                                             mtpt=mount_point))
            return True
        else:
            command = self.sshfs_cmd_tmpl.format(server=host,
                                                 dir_=path,
                                                 mtpt=mount_point)
            ret = subprocess.call(command, shell=True)
            if ret != 0:
                self.logger.error('Error mounting sshfs on %s@%s on %s',
                                  host, path, mount_point)
            return True if ret == 0 else False

    def umount_sshfs(self, mount_point):
        """Umount backup client filesytem after backup completed"""
        if self.debug:
            print("/bin/umount %s" % mount_point)
            return True
        else:
            ret = subprocess.call("/bin/umount %s" % mount_point, shell=True)
            if ret != 0:
                self.logger.error('Error umounting sshfs on %s', mount_point)
            return True if ret == 0 else False

    def get_backup_info(self, client):
        """ Gather list of includes and excludes, calcuate the paths of sources,
        mount sshfs if necessary
        return list of includes and excludes
        """
        includes = []
        excludes = []
        succeeded = []
        # Iternate each filesystems to be backup
        filesystems = self.job_conf.filesystems
        for fskey, fs_conf_dict in filesystems.items():
            new_conf = self.job_conf.raw_conf.copy()
            new_conf.update(fs_conf_dict)
            fs_conf = BackupConfig(new_conf,
                                   self.backup_type,
                                   self.debug,
                                   fskey=fskey)
            if not fs_conf.is_valid():
                self.logger.error('Invalid config')
                continue
            fs_type, fs_path = self.get_fstype_fspath(client,
                                                      fs_conf.path,
                                                      fs_conf.private_key)
            source_path = self.get_remote_path(fs_type,
                                               fs_conf.id_,
                                               fs_path,
                                               fs_conf.site)
            exp = fs_conf.exclude.replace(" ", "", )
            fs_conf.exclude = exp.replace("--exclude",
                                          "' --exclude '{}/".format(
                                              source_path))
            # Mount sshfs if not gluster
            if self.backup_type == "local" and fs_type != "nfs":
                mount_succeeded = self.mount_sshfs(client,
                                                   fs_conf.path,
                                                   source_path)
                succeeded.append(mount_succeeded)
            # Append includes and excludes lists
            includes.append(source_path)
            excludes.append(fs_conf.exclude)
        # Overall return succeed status
        return (all(succeeded), includes, excludes,
                fs_conf.backup_root, fs_conf.id_)

    def backup_host(self, client):
        """Runs backup process for each filesystem in config.
        Takes a dict "conf" with the following format:
        """

        def log_success(type_):
            """log backup success message and time"""
            log_tmpl = '%s of policy %s completed'
            msg_map = {
                'local': 'Backup',
                'offsite': 'Offsite',
                None: 'Unknown',
            }
            self.logger.info(log_tmpl, msg_map.get(type_), fsid)

        succeeded = []
        # Get includes and excludes and mount sshfs if needed
        ret, incl, excl, backup_root, fsid = self.get_backup_info(client)
        succeeded.append(ret)
        # Get borg backup repo of policy
        backup_repo = self.get_repo_path(backup_root, fsid)
        self.run_borgcheck_repo(backup_repo)
        # Run Borg backup
        backup_successed = self.create_backup(backup_repo,
                                              time.strftime("%Y%m%d"),
                                              " ".join(incl),
                                              "" if not "".join(excl)
                                              else "".join(excl)[1:] + "'")
        succeeded.append(backup_successed)
        succeeded.extend([
            self.umount_sshfs(mnt) for
            mnt in incl if
            'sshfs' in mnt
        ])
        # Prune borg archives
        succeeded.append(self.prune_archives(backup_repo,
                                             self.job_conf.daystokeep))
        all_succeeded = all(succeeded)
        if all_succeeded:
            log_success(self.backup_type)
        return all_succeeded

    def create_backup(self, repo, archive, includes, excludes):
        """Create borg backup archive base on repo and dates"""
        if self.debug:
            print(self.borgcreate_cmd_tmpl.format(repo=repo,
                                                  arch=archive,
                                                  includes=includes,
                                                  excludes=excludes))
            return True
        else:
            cmd = self.borgcreate_cmd_tmpl.format(repo=repo,
                                                  arch=archive,
                                                  includes=includes,
                                                  excludes=excludes)
            ret = subprocess.call(cmd,
                                  env=self.borg_env,
                                  shell=True)
            return True if ret == 0 else False

    def prune_archives(self, repo, daystokeep):
        """Prune archive older then days to keep"""
        if self.debug:
            print(self.borgprune_cmd_tmpl.format(repo=repo,
                                                 daystokeep=daystokeep))
            return True
        else:
            cmd = self.borgprune_cmd_tmpl.format(repo=repo,
                                                 daystokeep=daystokeep)
            ret = subprocess.call(cmd,
                                  env=self.borg_env,
                                  shell=True)
        return True if ret == 0 else False

    def run_backup(self):
        """
        Run all types of backup and return code to rundeck
        """
        if self.debug:
            print("run_backup\n")
            print(self.job_conf.raw_conf)
        succeeded = True
        for client in self.job_conf.hosts:
            # Run backup for each host
            succeeded = self.backup_host(client)
        return succeeded


def get_job_config(git_url, git_hash, job_folder, conf_name):
    """ Load JSON configs file from git
    git_url example:
    job_folder example: StorageOps
    """
    logger = logging.getLogger(__name__)
    conf_url = '%s%s/%s/%s' % \
               (git_url, git_hash, job_folder, conf_name)
    try:
        response = urllib.request.urlopen(conf_url)
    except:
        logger.error('Unable to open %s', conf_url)
        raise
    try:
        conf = json.loads(response.read().decode('utf-8'))
    except:
        logger.error('Unable to parse JSON in %s', conf_url)
        raise
    return conf


def cli_runner():
    """collect command line arguments and kick off run_backup"""
    parser = argparse.ArgumentParser(description='backup and \
                                     replicate policies')
    parser.add_argument('-l', '--lob', dest='line_of_business',
                        default=None, help='git directory',
                        required=True)
    parser.add_argument('-f', '--conf_name', dest='conf_name',
                        default=None, help='JSON config filename in git',
                        required=True)
    parser.add_argument('-v', '--git_hash', dest='git_hash',
                        default=None, help='Hash version of the file',
                        required=True)
    parser.add_argument('-t', '--backup_type', dest='backup_type',
                        default="local", help='Types of backup local/offsite',
                        required=False)
    parser.add_argument('-c', '--config_file', dest='config_file',
                        default="/etc/backup/backup.ini",
                        help='Backup server config file',
                        required=False)
    parser.add_argument('-d', '--debug', dest='debug',
                        default=False, help='True/False - turn debug on/off',
                        required=False)

    # Parsing arguements
    args = parser.parse_args()
    # Parsing backup server config
    config = configparser.ConfigParser()
    config.read(args.config_file)
    jobconf = {
        'borg_passphrase': config.get('backup_host', 'BORG_PASSPHRASE'),
        'borg_cache_dir': config.get('backup_host', 'BORG_CACHE_DIR'),
        'borg_files_cache_ttl': config.get('backup_host',
                                           'BORG_FILES_CACHE_TTL'),
        'tmpdir': config.get('backup_host', 'TMPDIR'),
        'private_key': config.get('backup_host', 'PRIVATE_KEY'),
        'local_path': config.get('backup_host', 'ONSITE_PATH'),
        'offsite_path': config.get('backup_host', 'OFFSITE_PATH'),
        'nfs_base_path': config.get('backup_host', 'nfs_base_path'),
        'sshfs_base_path': config.get('backup_host', 'sshfs_base_path'),
        'offsite': config.getboolean('job_defaults', 'offsite'),
        'repocheck': config.getboolean('job_defaults', 'repocheck'),
        'threads': config.get('job_defaults', 'threads'),
        'exclude': config.get('job_defaults', 'exclude')
    }
    giturl = config.get('job_config', 'GIT_URL')
    jobfolder = args.line_of_business
    jobconf.update(get_job_config(giturl,
                                  args.git_hash,
                                  jobfolder,
                                  args.conf_name))
    # Pass backup server config into job config
    jobconf['private_key'] = config.get('backup_host', 'PRIVATE_KEY')
    backup_processor = BackupProcessor(jobconf, args.backup_type, args.debug)
    return backup_processor.run_backup()


if __name__ == "__main__":
    if cli_runner():
        sys.exit(0)
    else:
        sys.exit(1)
