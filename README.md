# Backup with Borg
This is the codebase for linux backup base on [BorgBackup](https://borgbackup.readthedocs.io/en/stable/).

BorgBackup replaced symantec netbackup. It is a deduplicating backup program. We also leveraging compression and authenticated encryption and restore via fuse.

You can refer to [Data Backup Procedure](http://wiki.shopzilla.com/index.php?title=Data_Backup_Procedure) for operational steps.

---
## Architecture
BorgBackup does not utilize any database to catalog backups. It has a cache folder for fast access of recent backups and store backup into a hash of files which aggregate as a fuse for restore.

There is no backup client software for Borg. 

BorgBackup by design backup directories locally from where it runs(backup server). We therefore create backup jobs mapping backup clients filesystems to backup server with the following scenarios: 

### Local/Direct Attach Storage(DAS) 
local filesystems e.g. `/, /var, /opt` are mounted via sshfs on `/sshfs/<site>/<hostname>/<path>`

### Network Attached Storage(NAS)
When backup clients mounting gluster over NFS, backup maps corresponding gluster filesystems on backup servers. glusterfs will be backup locally from backup server.

### Components
- backup.py
  - Kickoff backup job by rundecks base on hostname and map client's filesystem for borg
- offsite.py
  - Kickoff offsite job by rundecks which create LVM snapshots of where borg backups locate
  - Replicate LVM snapshots with rclone to google storage

## Configuration
etc/backup/backup.ini

## How to Update backup schedules
Refer to [backup schedules](http://gitlab.shopzilla.com/configuration-management/backup-schedules)

## TODO
1. Borg backup to remote
2. replicate borg repo storage(site replication)
3. Implement borg check according

## Contact 
Any problem/questions, email **peter.auyeung@gmail.com** 
